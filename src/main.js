// packages
import Vue from 'vue'
import * as firebase from 'firebase'
import router from './router'
import { store } from './store'
// filters
import DateFilter from './filters/date'
// components
import EditPartyDetailsDialog from './components/parties/edit/edit-party-details-dialog.vue'
import App from './App.vue'

Vue.filter('date', DateFilter)
Vue.component('edit-party-details-dialog', EditPartyDetailsDialog)

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
    created() {
        firebase.initializeApp({
            apiKey: 'AIzaSyCOCd4SwVYUrKXKX0q_EMZhmYDW7NIkMVg',
            authDomain: 'underground-2317.firebaseapp.com',
            databaseURL: 'https://underground-2317.firebaseio.com',
            projectId: 'underground-2317',
            storageBucket: 'gs://underground-2317.appspot.com',
            messagingSenderId: '482807644248'
        })
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.$store.dispatch('autoSignIn', user)
                this.$store.dispatch('fetchUserData')
            }
        })
        this.$store.dispatch('loadParties')
    }
})
