import Vue          from 'vue'
import Router       from 'vue-router'
import AuthGuard    from './auth-guard'

import PageNotFound from '../components/page/page-not-found.vue'
import PageHome     from '../components/page/page-home.vue'
import PageMusic    from '../components/page/page-music.vue'
import PageParties  from '../components/page/page-parties.vue'
import PageUsers    from '../components/page/page-users.vue'
import UserSignup   from '../components/user/user-signup.vue'
import UserSignin   from '../components/user/user-signin.vue'
import UserProfile  from '../components/user/user-profile.vue'
import CreateParty  from '../components/parties/create-party.vue'
import SingleParty  from '../components/parties/single-party.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '*',
            name: 'PageNotFound',
            component: PageNotFound
        },
        {
            path: '/',
            name: 'PageHome',
            component: PageHome
        },
        {
            path: '/music',
            name: 'PageMusic',
            component: PageMusic
        },
        {
            path: '/parties',
            name: 'PageParties',
            component: PageParties
        },
        {
            path: '/party/new',
            name: 'CreateParty',
            component: CreateParty,
            beforeEnter: AuthGuard
        },
        {
            path: '/parties/:id',
            name: 'SingleParty',
            props: true,
            component: SingleParty
        },
        {
            path: '/users',
            name: 'PageUsers',
            component: PageUsers
        },
        {
            path: '/profile',
            name: 'UserProfile',
            component: UserProfile,
            beforeEnter: AuthGuard
        },
        {
            path: '/signup',
            name: 'UserSignup',
            component: UserSignup
        },
        {
            path: '/signin',
            name: 'UserSignin',
            component: UserSignin
        }
    ],
    mode: 'history'
})
