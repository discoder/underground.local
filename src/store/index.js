import Vue  from 'vue'
import Vuex from 'vuex'

import states  from './modules/states'
import user    from './modules/user'
import parties from './modules/parties'

Vue.use(Vuex)

export const store = new Vuex.Store({
    modules: {
        states,
        user,
        parties
    }
});
