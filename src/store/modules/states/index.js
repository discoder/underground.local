import * as types from './mutation-types'

const state = {
    overlayActive: false,
    loadingActive: false,
    mainNavigationActive: false

}

const mutations = {
    [types.SET_OVERLAY_ACTIVE] (state, payload) {
        state.overlayActive = payload
    },

    [types.SET_LOADING_ACTIVE] (state, payload) {
        state.loadingActive = payload
    },

    [types.SET_MAIN_NAVIGATION_ACTIVE] (state, payload) {
        state.mainNavigationActive = payload
    }
}

const actions = {
    setOverlayActive ({ state, commit }, payload) {
        commit(types.SET_OVERLAY_ACTIVE, payload)
    },

    setLoadingActive ({ state, commit }, payload) {
        commit(types.SET_LOADING_ACTIVE, payload)
    },

    setMainNavigationActive ({ state, commit }, payload) {
        commit(types.SET_MAIN_NAVIGATION_ACTIVE, payload)
    }
}

const getters = {
    overlayActive (state) {
        return state.overlayActive
    },

    loadingActive (state) {
        return state.loadingActive
    },

    mainNavigationActive (state) {
        return state.mainNavigationActive
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}
