import * as types from './mutation-types'
import * as typesStates from '../states/mutation-types.js'
import * as firebase from 'firebase'

const state = {
    user: null,
    authError: null // user auth error object
}

const mutations = {
    [types.SET_USER] (state, payload) {
        state.user = payload
    },

    [types.SET_AUTH_ERROR] (state, payload) {
        state.authError = payload
    },

    [types.CLEAR_AUTH_ERROR] (state, payload) {
        state.authError = null
    },

    [types.INVOLVED_USER_FOR_PARTY] (state, payload) {
        if ( state.user.registeredParties.findIndex(party => party.id === payload.id) >=0 ) {return}
        state.user.registeredParties.push(payload.id)
        state.user.firebaseKeys[payload.id] = payload.firebaseKey
    },

    [types.UNINVOLVED_USER_FROM_PARTY] (state, payload) {
        const registeredParties = state.user.registeredParties
        registeredParties.splice(registeredParties.findIndex(party => party.id === payload), 1)
        Reflect.deleteProperty(state.user.firebaseKeys, payload)
    }
}

const actions = {
    signUserUp ({commit}, payload) {
        commit(typesStates.SET_LOADING_ACTIVE, true)
        commit(types.CLEAR_AUTH_ERROR)
        firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
            .then(user => {
                const newUser = {
                    id: user.uid,
                    registeredParties: [],
                    firebaseKeys: {}
                }
                commit(types.SET_USER, newUser)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
            .catch(error => {
                commit(types.SET_AUTH_ERROR, error)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
    },

    signUserIn ({commit}, payload) {
        commit(typesStates.SET_LOADING_ACTIVE, true)
        commit(types.CLEAR_AUTH_ERROR)
        firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
            .then(user => {
                const newUser = {
                    id: user.uid,
                    registeredParties: [],
                    firebaseKeys: {}
                }
                commit(types.SET_USER, newUser)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
            .catch(error => {
                commit(types.SET_AUTH_ERROR, error)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
    },

    autoSignIn ({commit}, payload) {
        commit(types.SET_USER, {
            id: payload.uid,
            registeredParties: [],
            firebaseKeys: {}
        })
    },

    fetchUserData ({commit, getters}) {
        commit(typesStates.SET_LOADING_ACTIVE, true)
        firebase.database().ref('/users/' + getters.user.id + '/registrations/').once('value')
            .then(data => {
                const dataPairs = data.val()
                let registeredParties = []
                let swappedPairs = {}
                for (let key in dataPairs) {
                    registeredParties.push(dataPairs[key])
                    swappedPairs[dataPairs[key]] = key
                }
                // console.log(registeredParties)
                // console.log(swappedPairs)
                const updaterUser = {
                    id: getters.user.id,
                    registeredParties: registeredParties,
                    firebaseKeys: swappedPairs
                }
                commit(types.SET_USER, updaterUser)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
            .catch(error => {
                console.log(error)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
    },

    logout ({commit}) {
        firebase.auth().signOut()
        commit(types.SET_USER, null)
    },

    involvedUserForParty ({commit, getters}, payload) {
        commit(typesStates.SET_LOADING_ACTIVE, true)
        const user = getters.user
        firebase.database().ref('/users/' + user.id).child('/registrations/')
            .push(payload)
            .then(data => {
                commit(types.INVOLVED_USER_FOR_PARTY, {id: payload, firebaseKey: data.key})
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
            .catch(error => {
                console.log(error)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
    },

    uninvolvedUserFromParty ({commit, getters}, payload) {
        commit(typesStates.SET_LOADING_ACTIVE, true)
        const user = getters.user
        if (!user.firebaseKeys) {return}
        const firebaseKey = user.firebaseKeys[payload]
        firebase.database().ref('/users/' + user.id + '/registrations/').child(firebaseKey)
            .remove()
            .then(() => {
                commit(types.UNINVOLVED_USER_FROM_PARTY, payload)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
            .catch(error => {
                console.log(error)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
    }
}

const getters = {
    user (state) {
        return state.user
    },

    authError (state) {
        return state.authError
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}
