import * as types from './mutation-types'
import * as typesStates from '../states/mutation-types.js'
import * as firebase from 'firebase'

const state = {
    loadedParties: []
}

const mutations = {
    [types.CREATE_PARTY] (state, payload) {
        state.loadedParties.push(payload)
    },

    [types.UPDATE_PARTY] (state, payload) {
        const party = state.loadedParties.find(party => {
            return party.id === payload.id
        })
        if (payload.title) {
            party.title = payload.title
        }
        if (payload.description) {
            party.description = payload.description
        }
    },

    [types.SET_LOADED_PARTIES] (state, payload) {
        state.loadedParties = payload
    }
}

const actions = {
    loadParties ({commit}) {
        commit(typesStates.SET_LOADING_ACTIVE, true)
        firebase.database().ref('parties').once('value')
            .then((response) => {
                const parties = []
                const obj = response.val()
                for (let key in obj) {
                    parties.push({
                        id: key,
                        title: obj[key].title,
                        location: obj[key].location,
                        description: obj[key].description,
                        imageUrl: obj[key].imageUrl,
                        date: obj[key].date,
                        creatorId: obj[key].creatorId
                    })
                }
                commit(types.SET_LOADED_PARTIES, parties)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
            .catch((error) => {
                console.log(error)
                commit(typesStates.SET_LOADING_ACTIVE, false)
            })
    },

    createParty ({commit, getters}, payload) {
        const party = {
            title: payload.title,
            location: payload.location,
            description: payload.description,
            binaryImage: payload.binaryImage,
            date: payload.date.toISOString(),
            creatorId: getters.user.id
        }
        let imageUrl,
            key;
        firebase.database().ref('parties').push(party)
            .then((response) => {
                key = response.key // this is - PartyID. Set by the Firebase when creating a party
                return key
            })
            .then(key => {
                const filename = payload.binaryImage.name
                const ext = filename.slice(filename.lastIndexOf('.'))
                return firebase.storage().ref('parties/' + key + '.' + ext).put(payload.binaryImage)
            })
            .then(fileData => {
                imageUrl = fileData.metadata.downloadURLs[0]
                firebase.database().ref('parties').child(key).update({imageUrl: imageUrl})
            })
            .then(() => {
                commit(types.CREATE_PARTY, {
                    ...party,
                    imageUrl: imageUrl,
                    id: key
                })
            })
            .catch((error) => {
                console.log(error)
            })
    },

    updateParty ({commit}, payload) {
        commit(typesStates.SET_LOADING_ACTIVE, true)
        const updateObj = {}
        if (payload.title) {
            updateObj.title = payload.title
        }
        if (payload.description) {
            updateObj.description = payload.description
        }
        firebase.database().ref('parties').child(payload.id).update(updateObj)
        .then(() => {
            commit(types.UPDATE_PARTY, payload)
            commit(typesStates.SET_LOADING_ACTIVE, false)
        })
        .catch(error => {
            console.log(error)
            commit(typesStates.SET_LOADING_ACTIVE, false)
        })
    }
}

const getters = {
    loadedParties (state) {
        return state.loadedParties.sort((partyA, partyB) => {
            return partyA.date < partyB.date
        })
    },

    featuredParties (state, getters) {
        return getters.loadedParties.slice(0, 5)
    },

    loadedParty (state) {
        return (partyId) => {
            return state.loadedParties.find((party) => {
                return party.id === partyId
            })
        }
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}
